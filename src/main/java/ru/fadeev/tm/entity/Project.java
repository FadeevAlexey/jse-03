package ru.fadeev.tm.entity;

import ru.fadeev.tm.util.ConsoleHelper;

import java.util.Date;
import java.util.UUID;

public class Project implements IEntity {

    private String name = "";

    private String description = "";

    private String id = UUID.randomUUID().toString();

    private Date startDate = new Date();

    private Date finishDate = new Date();

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (!description.isEmpty())
            this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setStartDate(Date startDate) {
        if (startDate != null)
            this.startDate = startDate;
    }

    public void setFinishDate(Date finishDate) {
        if (finishDate != null)
            this.finishDate = finishDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + ConsoleHelper.printDate(startDate) +
                ", finishDate=" + ConsoleHelper.printDate(finishDate) +
                '}';
    }

}