package ru.fadeev.tm.entity;

import ru.fadeev.tm.util.ConsoleHelper;

import java.util.Date;
import java.util.UUID;

public class Task implements IEntity {

    private String name = "";

    private String description = "";

    private String id = UUID.randomUUID().toString();

    private Date startDate = new Date();

    private Date finishDate = new Date();

    private String projectId = "";

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        if (!description.isEmpty())
            this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStartDate(Date startDate) {
        if (startDate != null)
            this.startDate = startDate;
    }

    public void setFinishDate(Date finishDate) {
        if (finishDate != null)
            this.finishDate = finishDate;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + ConsoleHelper.printDate(startDate) +
                ", finishDate=" + ConsoleHelper.printDate(finishDate) +
                '}';
    }

}