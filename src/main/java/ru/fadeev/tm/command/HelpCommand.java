package ru.fadeev.tm.command;

import ru.fadeev.tm.bootstrap.Bootstrap;
import ru.fadeev.tm.enumerated.Operation;
import ru.fadeev.tm.service.CrudAble;

import java.util.Set;

public class HelpCommand implements ICommand {

    private Set<String> setCrudAble = Bootstrap.serviceFactory.crudAbleList();

    @Override
    public void execute(CrudAble service) {
        for (Operation operation : Operation.values()) {
            if (operation.getDescription().matches("%s:.*"))
                System.out.println(String.format(operation.getDescription()
                        , operation.name().toLowerCase()).replaceAll("_", "-"));
        }
        for (String crudAble : setCrudAble) {
            for (Operation operation : Operation.values()) {
                if (operation.getDescription().matches("%s-%s:.*"))
                    System.out.println(
                            String.format(operation.getDescription()
                                    , crudAble, operation.name().toLowerCase(), crudAble));
            }
        }
        System.out.println();
    }

}