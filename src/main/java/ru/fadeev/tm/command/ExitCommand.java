package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;

public class ExitCommand implements ICommand {

    @Override
    public void execute(CrudAble service) {
        System.out.println("Thank you for using our Task Manager");
    }

}