package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;

public class RemoveCommand implements ICommand {

    @Override
    public void execute(CrudAble service) {
        System.out.println(String.format("[%s REMOVE]", service.getName().toUpperCase()));
        System.out.println("ENTER NAME");
        service.remove();
        System.out.println("[OK]\n");
    }

}