package ru.fadeev.tm.command;

import ru.fadeev.tm.bootstrap.Bootstrap;
import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.util.ConsoleHelper;

public class TaskProjectCommand implements ICommand {

    @Override
    public void execute(CrudAble service) {
        service = Bootstrap.serviceFactory.getServiceByName("project");
        System.out.println("[ADD TASK TO PROJECT]");
        System.out.println("ENTER TASK NAME");
        String taskName = ConsoleHelper.readString();
        System.out.println("ENTER PROJECT NAME");
        String projectName = ConsoleHelper.readString();
        ProjectService projectService = (ProjectService) service;
        projectService.addIdToTask(taskName, projectName);
        System.out.println("[OK]\n");
    }

}