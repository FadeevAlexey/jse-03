package ru.fadeev.tm.command;

import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.util.ConsoleHelper;

public class ProjectTasksCommand implements ICommand {

    @Override
    public void execute(CrudAble service) {
        System.out.println("[PROJECT TASKS]");
        System.out.println("ENTER PROJECT NAME");
        String projectName = ConsoleHelper.readString();
        ProjectService project = (ProjectService) service;
        int i = 1;
        for (Task task : project.getProjectTasks(projectName)) {
            System.out.println(i++ + ". " + task);
        }
        System.out.println();
    }

}