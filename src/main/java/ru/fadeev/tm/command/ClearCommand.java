package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;

public class ClearCommand implements ICommand {

    @Override
    public void execute(CrudAble service) {
        service.clear();
        System.out.println(String.format("[ALL %ss REMOVE]\n", service.getName()));
    }

}