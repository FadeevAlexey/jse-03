package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;

public interface ICommand {

    void execute(CrudAble service);

}