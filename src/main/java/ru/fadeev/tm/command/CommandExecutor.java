package ru.fadeev.tm.command;

import ru.fadeev.tm.enumerated.Operation;
import ru.fadeev.tm.service.CrudAble;

import java.util.HashMap;
import java.util.Map;

public class CommandExecutor {

    private static Map<Operation, ICommand> allKnownCommandsMap = new HashMap<>();

    static {
        allKnownCommandsMap.put(Operation.CREATE, new CreateCommand());
        allKnownCommandsMap.put(Operation.LIST, new ListCommand());
        allKnownCommandsMap.put(Operation.HELP, new HelpCommand());
        allKnownCommandsMap.put(Operation.REMOVE, new RemoveCommand());
        allKnownCommandsMap.put(Operation.CLEAR, new ClearCommand());
        allKnownCommandsMap.put(Operation.EXIT, new ExitCommand());
        allKnownCommandsMap.put(Operation.EDIT, new EditCommand());
        allKnownCommandsMap.put(Operation.PROJECT_TASKS, new ProjectTasksCommand());
        allKnownCommandsMap.put(Operation.TASK_PROJECT, new TaskProjectCommand());
    }

    public static void execute(Operation operation, CrudAble service) {
        if (operation != null) {
            Map<Operation, ICommand> allKnownCommandsMap = CommandExecutor.allKnownCommandsMap;
            if (allKnownCommandsMap.containsKey(operation))
                allKnownCommandsMap.get(operation).execute(service);
        }
    }

}