package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.util.ConsoleHelper;

import java.util.Date;

public class EditCommand implements ICommand {

    @Override
    public void execute(CrudAble service) {
        System.out.println(String.format("[%s EDIT]", service.getName().toUpperCase()));
        System.out.println("ENTER NAME:");
        String name = ConsoleHelper.readString();
        System.out.println("YOU CAN ADD EDIT DESCRIPTION OR PRESS ENTER");
        String description = ConsoleHelper.readString();
        System.out.println("YOU CAN ADD EDIT START DATE OR PRESS ENTER");
        Date startDate = ConsoleHelper.readDate();
        System.out.println("YOU CAN ADD EDIT FINISH DATE OR PRESS ENTER");
        Date finishDate = ConsoleHelper.readDate();
        service.edit(name, description, startDate, finishDate);
        System.out.println("[OK]\n");
    }

}