package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.util.ConsoleHelper;

public class CreateCommand implements ICommand {

    @Override
    public void execute(CrudAble service) {
        System.out.println(String.format("[%s CREATE]", service.getName().toUpperCase()));
        System.out.println("ENTER NAME:");
        String name = ConsoleHelper.readString();
        service.create(name);
        System.out.println("[OK]\n");
        System.out.println(
                String.format(
                        "IF YOU WOULD LIKE EDIT PROPERTIES %s USE COMMAND %s-edit\n"
                        , service.getName().toUpperCase()
                        , service.getName().toLowerCase()));
    }

}