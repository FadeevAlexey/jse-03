package ru.fadeev.tm.bootstrap;

import ru.fadeev.tm.command.CommandExecutor;
import ru.fadeev.tm.enumerated.Operation;
import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.util.ConsoleHelper;

public class Bootstrap {

    public static ServiceFactory serviceFactory;

    public void init() {
        serviceFactory = new ServiceFactory();
        serviceFactory.init();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        listener();
    }

    public void listener() {
        Operation operation = null;
        do {
            try {
                String[] managerAndCommand = ConsoleHelper.getManagerAndCommand();
                operation = Operation.getAllowableOperation(managerAndCommand[1]);
                CrudAble services = serviceFactory.getServiceByName(managerAndCommand[0]);
                CommandExecutor.execute(operation, services);
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
            }
        } while (operation != Operation.EXIT);
    }

}