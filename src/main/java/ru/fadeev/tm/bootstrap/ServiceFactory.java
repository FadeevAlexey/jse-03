package ru.fadeev.tm.bootstrap;

import ru.fadeev.tm.service.CrudAble;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.service.TaskService;

import java.util.*;

public class ServiceFactory {

    public Map<String, CrudAble> map = new LinkedHashMap<>();

    public void init() {
        map.put("project", new ProjectService());
        map.put("task", new TaskService());
    }

    public CrudAble getServiceByName(String nameOfService) {
        if (nameOfService == null || nameOfService.isEmpty())
            return null;
        return map.get(nameOfService);
    }

    public Set<String> crudAbleList() {
        return map.keySet();
    }

}