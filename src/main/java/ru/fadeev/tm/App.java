package ru.fadeev.tm;

import ru.fadeev.tm.bootstrap.Bootstrap;

public class App {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}