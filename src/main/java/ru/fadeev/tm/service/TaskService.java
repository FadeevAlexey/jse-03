package ru.fadeev.tm.service;

import ru.fadeev.tm.entity.IEntity;
import ru.fadeev.tm.util.ConsoleHelper;
import ru.fadeev.tm.entity.Task;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class TaskService implements CrudAble {

    List<Task> tasks = new LinkedList<>();

    @Override
    public void create(String name) {
        Task task = new Task();
        task.setName(name);
        tasks.add(task);
    }

    @Override
    public List<IEntity> list() {
        return new LinkedList<>(tasks);
    }

    @Override
    public void clear() {
        tasks = new LinkedList<>();
    }

    @Override
    public void remove() {
        String tasksName = ConsoleHelper.readString();
        tasks.removeIf(task -> task.getName().equals(tasksName));
    }

    @Override
    public String getName() {
        return Task.class.getSimpleName();
    }

    @Override
    public void edit(String name, String description, Date startDate, Date finishDate) {
        Optional<Task> optionalTask = tasks.stream().filter(pr -> pr.getName().equals(name)).findAny();
        if (optionalTask.isPresent()) {
            Task task = optionalTask.get();
            task.setDescription(description);
            task.setStartDate(startDate);
            task.setFinishDate(finishDate);
        }
    }

    public void removeTasksByProjectId(String id) {
        tasks.removeIf(project -> project.getProjectId().equals(id));
    }

}