package ru.fadeev.tm.service;

import ru.fadeev.tm.bootstrap.Bootstrap;
import ru.fadeev.tm.entity.IEntity;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.util.ConsoleHelper;
import ru.fadeev.tm.entity.Project;

import java.util.*;
import java.util.stream.Collectors;

public class ProjectService implements CrudAble {

    private List<Project> projects = new LinkedList<>();

    @Override
    public void create(String projectName) {
        Project project = new Project();
        project.setName(projectName);
        projects.add(project);
    }

    @Override
    public List<IEntity> list() {
        return new LinkedList<>(projects);
    }

    @Override
    public void clear() {
        projects = new LinkedList<>();
        getTasks().tasks.removeIf(task -> !task.getProjectId().isEmpty());
    }

    @Override
    public void remove() {
        String name = ConsoleHelper.readString();
        Iterator<Project> iterator = projects.iterator();
        while (iterator.hasNext()) {
            Project project = iterator.next();
            if (project.getName().equals(name)) {
                getTasks().removeTasksByProjectId(project.getId());
                iterator.remove();
                return;
            }
        }
        throw new IllegalArgumentException("Can't find project");
    }

    @Override
    public String getName() {
        return Project.class.getSimpleName();
    }

    @Override
    public void edit(String name, String description, Date startDate, Date finishDate) {
        Optional<Project> optionalProject = projects.stream().filter(pr -> pr.getName().equals(name)).findAny();
        if (optionalProject.isPresent()) {
            Project project = optionalProject.get();
            project.setDescription(description);
            project.setStartDate(startDate);
            project.setFinishDate(finishDate);
        }
    }

    String getIdByName(String name) {
        for (Project project : projects) {
            if (project.getName().equals(name))
                return project.getId();
        }
        throw new IllegalArgumentException("Can't find project with name: " + name);
    }

    String getNameById(String id) {
        for (Project project : projects) {
            if (project.getId().equals(id))
                return project.getName();
        }
        throw new IllegalArgumentException("Can't find project with id: " + id);
    }

    public TaskService getTasks() {
        return (TaskService) Bootstrap.serviceFactory.getServiceByName("task");
    }

    public List<Task> getProjectTasks(String name) {
        return getTasks().tasks
                .stream()
                .filter(task -> task.getProjectId().equals(getIdByName(name)))
                .collect(Collectors.toList());
    }

    public void addIdToTask(String taskName, String projectName) {
        Optional<Task> task = getTasks().tasks
                .stream()
                .filter(tsk -> tsk.getName().equals(taskName))
                .findAny();
        task.ifPresent(value -> value.setProjectId(getIdByName(projectName)));
    }

}