package ru.fadeev.tm.service;

import ru.fadeev.tm.entity.IEntity;

import java.util.Date;
import java.util.List;

public interface CrudAble {

    void clear();

    void remove();

    String getName();

    List<IEntity> list();

    void create(String name);

    void edit(String name, String description, Date startDate, Date finishDate);

}